# CPLDICY

Commodore Amiga 2000/3000/4000 I2C controller as Zorro-II card. This controller is software compatible to M. Boehmer's ICY board.
The main difference between the original ICY card and this design is that the PCF8584 I2C controller chip is mapped indirectly to the Amiga bus here. This way, occasional mis-configuration of the PCF8584 bus protocol and timing issues can be worked around.

The logic for the CPLD is based on the code for the A3000 daughterboard by Matthias Heinrichs, with some changes and additions to make it work on a separate Zorro card. The KiCAD footprint for the Zorro slot was imported from the [VA2000](https://github.com/mntmn/amiga2000-gfxcard) project by Lukas Hartmann.

## Features

While the main purpose of this board is to provide an I2C controller, there are some optional on-board extras. 

One option is a PWM fan controller that might be beneficial for finding a good balance between noise and cooling. 
This fan controller is compatible with the [Fanny](https://gitlab.com/HenrykRichter/i2csensors/-/tree/master/Fanny) card
and can be configured with the utility provided there.

The second option is a temperature/voltage monitor, based on the LTC2990. That monitor is configured in the same
fashion as on the a1k.org ICYv2 card and Matze's A3000DB and is supported out of the box by several utilities, e.g. [simplesensors](https://gitlab.com/HenrykRichter/i2csensors/-/tree/master/sensors) or [Sensei](https://aminet.net/package/driver/other/Sensei).

## Pictures

The picture below shows Revision 0.3 of the board. 
![pic1](https://gitlab.com/HenrykRichter/cpldicy/raw/master/Pics/CPLDICY1.JPG)


## Building

The PCB is a two layer design. Gerber files for production can be found in the corresponding sub-folder.

A BOM is provided in the PCB folder in .xlsx format. It also contains Mouser part numbers. Please refer to the schematics for the essential parts and optional parts.

The CPLD configuration file is to be found in the Logic/ subfolder in JEDEC format. Please note that the latest version states "1CEDECAF" as AutoConfig serial number. Previous releases contained "1CECAFFE" as serial number. 

## 
**DISCLAIMER!!!!**
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA OR LEADS TO DAMAGED COMPUTERS!**

## License
The PCB and it's design files are licensed as [CC BY NC SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en).
