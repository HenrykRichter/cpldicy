----------------------------------------------------------------------------------
-- Company:  a1k.org
-- Engineer: Matthias Heinrichs (AA3000DB), adapted for CPLDICY by Henryk Richter
-- 
-- Create Date:    22:17:23 10/18/2018 
-- Design Name: 
-- Module Name:    IcyCPLD - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: Amiga Zorro support for PCF8584
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
--          0.02 - added CFGOUT logic (1CECAFFE)
--          0.03 - small fixes (1CEDECAF)
--          0.04 - fixed A21/A22 mismatch (1CE1CEBB)
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IcyCPLD is
    Port ( A : in  STD_LOGIC_VECTOR (23 downto 1);
           D : inout  STD_LOGIC_VECTOR (15 downto 8);
           nDS : in  STD_LOGIC;
           BD : inout  STD_LOGIC_VECTOR (7 downto 0);
           BA1 : out  STD_LOGIC;
           nCF_IN : in  STD_LOGIC;
           nCF_OUT : out  STD_LOGIC;
           nSLAVE_IN : in  STD_LOGIC;
           nSLAVE_OUT : out  STD_LOGIC;
           RESET : in  STD_LOGIC;
           nCS : out  STD_LOGIC;
           RW : in  STD_LOGIC;
           nAS : in  STD_LOGIC;
           nDTACK : out  STD_LOGIC;
           CTRL : in  STD_LOGIC;
           E7M : in  STD_LOGIC;
           CDAC : in  STD_LOGIC;
           DISABLE : in  STD_LOGIC;
           nOVR : out  STD_LOGIC;
           nBERR : out  STD_LOGIC);
end IcyCPLD;

architecture Behavioral of IcyCPLD is
signal	ICY_CYCLE: STD_LOGIC;
signal	ICY_ENABLED: STD_LOGIC;
signal	AUTO_CONFIG:STD_LOGIC;
signal	AUTO_CONFIG_DONE:STD_LOGIC;
signal	AUTO_CONFIG_DONE_CYCLE:STD_LOGIC;
signal	SHUT_UP:STD_LOGIC;
signal	BASEADR:STD_LOGIC_VECTOR(7 downto 0);
signal	Dout1:STD_LOGIC_VECTOR(3 downto 0);
signal	nAS_D0:STD_LOGIC;
signal	clk:STD_LOGIC;

begin

	clk <= '1' when E7M = CDAC else '0'; --14MHz Clock
	
	D(15 downto 8) <= Dout1 & Dout1 when AUTO_CONFIG= '1' and nAS='0' and RW='1' else
							BD when ICY_CYCLE ='1' and nAS='0' and RW='1'  else
							"ZZZZZZZZ";
	BD					<= D(15 downto 8) when ICY_CYCLE ='1' and nAS='0' and RW='0'  and nDS='0' else
							"ZZZZZZZZ";

--	nBERR <= 		'0' when ICY_CYCLE ='1' and nSLAVE_IN = '0' else 
--						'Z';
	nBERR <= 'Z';
--	nSLAVE_OUT <= 	'0' when ((ICY_CYCLE ='1' or AUTO_CONFIG ='1') and nAS='0') or nSLAVE_IN = '0' else '1';
	nSLAVE_OUT <=   '0' when ((ICY_CYCLE ='1' or AUTO_CONFIG ='1') and nAS='0') else '1';
	--nSLAVE_OUT <= 	'Z';
	nOVR <= 			'0' when (ICY_CYCLE ='1' or AUTO_CONFIG ='1') and nAS='0' else 
						'Z';
	nDTACK <= 		'0' when AUTO_CONFIG ='1' and nAS='0' else 
						'0' when ICY_CYCLE ='1' and nAS='0' 
									and (CTRL ='0' or (RW='1' and ICY_ENABLED='0')) --normal DTACK from PT or read without first wrtie
						else 'Z';
	BA1 <= A(1);
	
	nCF_OUT <= '0' when AUTO_CONFIG_DONE = '1' else nCF_IN when DISABLE='0' else '1'; -- nCF_OUT <= not AUTO_CONFIG_DONE;
	
	
	i2control: process ( reset,clk)
	begin
		if	reset = '0' then
			-- reset active ...
			ICY_CYCLE <='0';
			ICY_ENABLED <='0';
			nCS <='1';
		elsif rising_edge(clk) then -- no reset, so wait for rising edge of the clock
			--decode icy-cyle
			if(A(23 downto 16) = BASEADR
				and AUTO_CONFIG_DONE = '1' and SHUT_UP ='0')then
				ICY_CYCLE <='1';
			else
				ICY_CYCLE <='0';
			end if;

			--decode first write to enable reads
			if(ICY_CYCLE ='1' and nAS='0' and RW='0')then
				ICY_ENABLED <='1';
			end if;

			--make a chip select
			if (( (ICY_CYCLE ='1' and RW='0' ) or --icy write
				 (ICY_CYCLE ='1' and RW='1' and ICY_ENABLED='1')) --icy read with enabled flag
				and nAS='0' and nDS='0') then
				nCS <= '0';
			else 
				nCS <= '1';
			end if;
		end if;
	end process;
	
	
	autoconfig: process (reset, clk)
	begin
		if	reset = '0' then
			-- reset active ...
			Dout1<="1111";
			SHUT_UP	<='1';
			BASEADR<=x"FF";
			AUTO_CONFIG	<= '0';
			nAS_D0 <='1';			
			AUTO_CONFIG_DONE_CYCLE	<='0';
			AUTO_CONFIG_DONE	<='0';

		elsif rising_edge(clk) then -- no reset, so wait for rising edge of the clock		
			
			
			--decode ac-access
			if(A(23 downto 16) = x"E8"
				and AUTO_CONFIG_DONE = '0' 
				and nCF_IN ='0' 
				and DISABLE = '1'
				)then
				AUTO_CONFIG	<= '1';
			else
				AUTO_CONFIG	<= '0';
			end if;

			--delay some signals for edge detection
			nAS_D0				<=nAS;			
			
			--ac done on end of cycle
			if(nAS='1' and nAS_D0='0')then
				AUTO_CONFIG_DONE <= AUTO_CONFIG_DONE_CYCLE;
			end if;
			
			--default values (will be ovewritten later)
			Dout1<="1111";

			
			if(AUTO_CONFIG= '1' and nAS='0') then
				case A(6 downto 1) is
					when "000000"	=> 	Dout1 <= "1100" ; --ZII,  no Memory, no ROM
					when "000001"	=> 	Dout1 <=	"0001" ; --one Card, 64kb = 001
					when "000011"	=> 	Dout1 <=	"0000" ; --ProductID low nibble: F->0000
					when "001000"	=> 	Dout1 <=	"1110" ; --Ventor ID 0
					when "001001"	=> 	Dout1 <=	"1100" ; --Ventor ID 1
					when "001010"	=> 	Dout1 <=	"0111" ; --Ventor ID 2
					when "001011"	=> 	Dout1 <=	"0110" ; --Ventor ID 3 : $0A1C: A1K.org
					when "001100"	=> 	Dout1 <=	"1110" ; --Serial byte 0 (msb) high nibble
					when "001101"	=> 	Dout1 <=	"0011" ; --Serial byte 0 (msb) low  nibble
					when "001110"	=> 	Dout1 <=	"0001" ; --Serial byte 1       high nibble
					--
					when "001111"	=> 	Dout1 <=	"1110" ; --Serial byte 1       low  nibble
					when "010000"	=> 	Dout1 <=	"0011" ; --Serial byte 2       high nibble
					when "010001"	=> 	Dout1 <=	"0001" ; --Serial byte 2       low  nibble
					when "010010"	=> 	Dout1 <=	"0100" ; --Serial byte 3       high nibble
					when "010011"	=> 	Dout1 <=	"0100" ; --Serial byte 3 (lsb) low  nibble: 1CE1CEBB
--					when "001111"	=> 	Dout1 <=	"0010" ; --Serial byte 1       low  nibble
--					when "010000"	=> 	Dout1 <=	"0001" ; --Serial byte 2       high nibble
--					when "010001"	=> 	Dout1 <=	"0011" ; --Serial byte 2       low  nibble
--					when "010010"	=> 	Dout1 <=	"0101" ; --Serial byte 3       high nibble
--					when "010011"	=> 	Dout1 <=	"0000" ; --Serial byte 3 (lsb) low  nibble: 1CEDECAF
--					when "001111"	=> 	Dout1 <=	"0011" ; --Serial byte 1       low  nibble
--					when "010000"	=> 	Dout1 <=	"0101" ; --Serial byte 2       high nibble
--					when "010001"	=> 	Dout1 <=	"0000" ; --Serial byte 2       low  nibble
--					when "010010"	=> 	Dout1 <=	"0000" ; --Serial byte 3       high nibble
--					when "010011"	=> 	Dout1 <=	"0001" ; --Serial byte 3 (lsb) low  nibble: 1CECAFFE
					when others		=> 	Dout1 <=	"1111" ;
				end case;	

				if( RW='0' and nDS='0')then --write
					if(A (6 downto 1)="100100")then	--read AC-Address							
						BASEADR 				<= D(15 downto 8); --Base adress
						AUTO_CONFIG_DONE_CYCLE	<='1'; --done here
						SHUT_UP				<='0'; --enable board
					elsif(A (6 downto 1)="100110")then --shutup!
						AUTO_CONFIG_DONE_CYCLE	<='1'; --done here
					end if;
				end if;
			end if;
		end if;
	end process autoconfig; --- that's all
end Behavioral;

